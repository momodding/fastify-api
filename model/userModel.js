const { Model } = require('objection');
const knex = require('../config/connection');
const PhoneModel = require('../model/phoneModel');

Model.knex(knex);

class UserModel extends Model {
    static get tableName() {
        return 'users';
    }

    static get relationMappings() {
        return {
            phone: {
                relation: Model.HasManyRelation,
                modelClass: PhoneModel,
                join: {
                    from: 'users.id',
                    to: 'phone.user_id'
                }
            }
        };
    }
}

module.exports = UserModel;