-- CREATE USER 'root'@'%' IDENTIFIED BY 'password';

-- GRANT INSERT, CREATE, ALTER, UPDATE, SELECT, REFERENCES on database_development.*
-- TO 'root'@'%' IDENTIFIED BY 'password'
-- WITH GRANT OPTION;

-- CREATE DATABASE IF NOT EXISTS database_development;
CREATE SCHEMA
'database_development' DEFAULT CHARACTER
SET utf8;

GRANT ALL PRIVILEGES on database_development.* TO 'root'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;